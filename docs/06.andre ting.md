Andre ting
--

Namespace
---
```typescript
namespace Avento.Core {
    
}
```

Classes
---
```typescript
class Foo {

}
```

Functions
---
```typescript
function add(x, y) {
    return x + y;
}

let myAdd = function(x, y) { return x+y; };
```

Interfaces
---

```typescript
interface IFoo {
    bar(count:number);
}
```

Generics
---
```typescript

class Foo<T> {

}

interface IFoo<T> {
    bar(x:T);
}

function bar<T>(x:T) : T {
    return x;
}    
    
```